package org.meveo.admin.action.document;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Named;

@Named
@ConversationScoped
public class AccountEntityListBean extends AccountEntityBean {

    private static final long serialVersionUID = 5761299788298195322L;
}