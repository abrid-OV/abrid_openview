/*
 * (C) Copyright 2015-2016 Opencell SAS (http://opencellsoft.com/) and contributors.
 * (C) Copyright 2009-2014 Manaty SARL (http://manaty.net/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 * This program is not suitable for any direct or indirect application in MILITARY industry
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.meveo.admin.action.document;

import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Named;

import org.meveo.admin.action.BaseBean;
import org.meveo.admin.exception.BusinessException;
import org.meveo.documents.AccountingFirm;
import org.meveo.model.admin.AccountType.AccountTypeEnum;
import org.meveo.service.account.AccountTypeService;
import org.meveo.service.base.local.IPersistenceService;
import org.meveo.service.document.AccountingFirmService;
import org.omnifaces.cdi.ViewScoped;
import org.primefaces.model.LazyDataModel;

@Named
@ViewScoped
public class AccountingFirmBean extends BaseBean<AccountingFirm> {

    private static final long serialVersionUID = -2634473401398553083L;

  
    @Inject
    private AccountingFirmService accountingFirmService;
    
    @Inject
    private AccountTypeService accountTypeService;

    /**
     * Constructor. Invokes super constructor and provides class type of this bean for {@link BaseBean}.
     */
    
    public AccountingFirmBean() {
   	 super(AccountingFirm.class);
   }
  
    
    @Override
	protected IPersistenceService<AccountingFirm> getPersistenceService() {
		return (IPersistenceService<AccountingFirm>) accountingFirmService;
	}
    
    


    @Override
    public String saveOrUpdate(boolean killConversation) throws BusinessException {
    	entity.setAccountType(accountTypeService.getAccountType(AccountTypeEnum.expertComptable)); 
    	return super.saveOrUpdate(killConversation);
    }
    
    

    public LazyDataModel<AccountingFirm> getAccountingFirms(){
    	if (filters == null){
    		filters = new HashMap<>();
    	} 
    	filters.put("accountType.accountTypeEnum",AccountTypeEnum.expertComptable);
    	return getLazyDataModel();
    }
	
	
  

}