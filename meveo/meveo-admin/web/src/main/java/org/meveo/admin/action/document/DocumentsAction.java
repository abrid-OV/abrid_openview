package org.meveo.admin.action.document;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.seam.international.status.Messages;
import org.jboss.solder.logging.Logger;
import org.meveo.admin.util.pagination.PaginationDataModel;
import org.meveo.services.repository.DocumentService;
import org.meveo.util.view.PaginationConfiguration;

import com.lowagie.text.Document;

@ConversationScoped
@Named
public class DocumentsAction  implements Serializable {


    /**
     * 
     */
    private static final long serialVersionUID = 5714791260420135724L;

    @Inject
    private Logger log;
    
    @Inject
    DocumentService documentService;
    
    @Inject
    private Messages messages;

    @Inject
    private Conversation conversation;

    String accountCode;
    String filename;
    
    private int pageSize = 20;

    private PaginationDataModel<Document> documentsDataModel;
    
    /***********************************************************************************/
    /* JSF INTERACTION */
    /***********************************************************************************/

    
    /**
     * ACTIONS
     */
    
    @Produces
    @ConversationScoped
    @Named("documentsDataModel")
    public PaginationDataModel<Document> find() {
        // if(currentUser.getBusinessAccount() instanceof CustomerAccount){
        // this.accountCode = ((CustomerAccount)currentUser.getBusinessAccount()).getRef1();
        // }

        if (documentsDataModel == null) {
            documentsDataModel = new DocumentsDataModel();
        }

        documentsDataModel.forceRefresh();

        return documentsDataModel;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        // if(currentUser.getBusinessAccount() instanceof CustomerAccount){
        // this.accountCode = ((CustomerAccount)currentUser.getBusinessAccount()).getRef1();
        // }

        this.accountCode = accountCode;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void refresh() {
        documentsDataModel = new DocumentsDataModel();
        find();
    }

    
    /***********************************************************************************/
    /* DATATABLE MODEL */
    /***********************************************************************************/
    class DocumentsDataModel extends PaginationDataModel<Document> {

        private static final long serialVersionUID = 1L;

//        @Override
//        protected int countRecords(PaginationConfiguration paginatingData) {
//            int documentCount = (int) documentService.count(accountCode,filename);
//            return documentCount;
//        }
//
//        @Override
//        protected List<Document> loadData(PaginationConfiguration configuration) {
//            return documentService.find(accountCode,filename,configuration);
//        }
    }
}
		