/*
 * (C) Copyright 2015-2016 Opencell SAS (http://opencellsoft.com/) and contributors.
 * (C) Copyright 2009-2014 Manaty SARL (http://manaty.net/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 * This program is not suitable for any direct or indirect application in MILITARY industry
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.meveo.admin.action.document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.meveo.admin.action.BaseBean;
import org.meveo.admin.exception.BusinessException;
import org.meveo.documents.Dossier;
import org.meveo.model.admin.AccountType.AccountTypeEnum;
import org.meveo.model.admin.User;
import org.meveo.service.account.AccountTypeService;
import org.meveo.service.admin.impl.UserService;
import org.meveo.service.base.local.IPersistenceService;
import org.meveo.service.document.DossierService;
import org.omnifaces.cdi.ViewScoped;
import org.primefaces.model.LazyDataModel;

@Named
@ViewScoped
public class DossierBean extends BaseBean<Dossier> {

    private static final long serialVersionUID = -2634473401398553083L;

  
    @Inject
    private DossierService dossierService;
    
    @Inject
    private UserService userService;
    
    @Inject
    private AccountTypeService accountTypeService;

    /**
     * Constructor. Invokes super constructor and provides class type of this bean for {@link BaseBean}.
     */
    
    public DossierBean() {
   	 super(Dossier.class);
   }
  
    
    @Override
	protected IPersistenceService<Dossier> getPersistenceService() {
		return (IPersistenceService<Dossier>) dossierService;
	}

    
	@Override
	public String saveOrUpdate(boolean killConversation) throws BusinessException {
       // entity.setAccountType(accountTypeService.getAccountType(AccountTypeEnum.dossier)); 
        return super.saveOrUpdate(killConversation);
    }
	
	 public List<User> getResponsiblesEc() {
	    	List<User> UserList = new ArrayList<User>(); 
	    	List<User> userMangers = new ArrayList<User>(); 
	    	UserList=userService.list();
	    	if(UserList.size()>0){
	    	for(User user:UserList){
	    		if(user.hasRole("serviceManager")){
	    			userMangers.add(user);
	    		}
	    	}}
	        return userMangers;
	    }
	 
	 
	 public List<User> getAssociates() {
	    	List<User> users = new ArrayList<User>();  
	    	List<User> userAssociats = new ArrayList<User>(); 
	    	users=userService.list();
	    	if(users.size()>0){
	    	for(User user:users){
	    		if(user.hasRole("associate")){
	    			userAssociats.add(user);
	    		}
	    	}}
	        return userAssociats;
	    }
	 
	 public List<User> getResponsibles() {
	    	List<User> allUsers= new ArrayList<User>();
	    	List<User> userResponsibles = new ArrayList<User>(); 
	    	allUsers=userService.list();
	    	if(allUsers.size()>0){
	    	for(User user:allUsers){
	    		if(user.hasRole("associate") || user.hasRole("accountant") ){
	    			userResponsibles.add(user);
	    		}
	    	}}
	        return userResponsibles;
	    }
	   public LazyDataModel<Dossier> getDossiers(){
			if (filters == null){
				filters = new HashMap<>();
			} 
			filters.put("accountType.accountTypeEnum",AccountTypeEnum.dossier);
			return getLazyDataModel();
		}
	
	
  

}