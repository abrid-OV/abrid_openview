package org.meveo.admin.action.document;
import java.sql.BatchUpdateException;

import javax.inject.Inject;
import javax.inject.Named;

import org.meveo.admin.action.BaseBean;
import org.meveo.model.admin.AccountEntity;
import org.meveo.service.base.PersistenceService;
import org.meveo.service.base.local.IPersistenceService;
import org.meveo.service.document.AccountEntityService;
import org.omnifaces.cdi.ViewScoped;

@Named
@ViewScoped
public class AccountEntityBean extends BaseBean<AccountEntity> {

    private static final long serialVersionUID = -2634473401390003093L;

    /**
     * Injected @{link Title} service. Extends {@link PersistenceService}.
     */
    @Inject
    private AccountEntityService accountEntityService;

    /**
     * Constructor. Invokes super constructor and provides class type of this bean for {@link BaseBean}.
     */
    public AccountEntityBean() {
        super(AccountEntity.class);
       
    }
    
    @Override
    public String getListViewName(){
    	return "accountEntities";
    }

    /**
     * @see org.meveo.admin.action.BaseBean#getPersistenceService()
     */
    @Override
    protected IPersistenceService<AccountEntity> getPersistenceService() {
        return (IPersistenceService<AccountEntity>) accountEntityService;
    }

    public void test() throws BatchUpdateException {
        throw new BatchUpdateException();
    }

}