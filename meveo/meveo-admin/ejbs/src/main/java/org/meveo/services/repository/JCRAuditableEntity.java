package org.meveo.services.repository;

import org.meveo.model.admin.User;
import org.meveo.model.jcr.JCREntity;

public abstract class JCRAuditableEntity extends JCREntity {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public abstract void updateAudit(User user);

}
