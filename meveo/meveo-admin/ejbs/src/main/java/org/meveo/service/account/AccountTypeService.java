package org.meveo.service.account;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.meveo.commons.utils.QueryBuilder;
import org.meveo.documents.AccountingFirm;
import org.meveo.model.admin.AccountType;
import org.meveo.model.admin.AccountType.AccountTypeEnum;
import org.meveo.util.MeveoJpa;
import org.primefaces.model.LazyDataModel;

public class AccountTypeService implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4100035764664765L;

    @Inject
    @MeveoJpa
    EntityManager em;

    @SuppressWarnings("unchecked")
    public AccountType getAccountType(AccountTypeEnum accountType) {

        String sql = "select distinct a from AccountType a";

        QueryBuilder qb = new QueryBuilder(sql);

        if (accountType != null) {
            qb.addCriterionEntity("a.accountTypeEnum", accountType);
        }

        List<AccountType> accountTypes = qb.find(em);
        return accountTypes.size() > 0 ? accountTypes.get(0) : null;
    }
    
   

}
