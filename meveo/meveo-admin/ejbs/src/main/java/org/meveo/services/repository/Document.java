package org.meveo.services.repository;

import java.io.InputStream;
import java.util.Calendar;

import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.ValueFactory;

import org.meveo.model.jcr.JCREntity;
import org.meveo.service.base.BusinessEntityException;

public class Document extends JCREntity {

	private static final long serialVersionUID = -503081163920441944L;

	private String filename;
	private InputStream inputStream;
	private String mimeType;
	private String category;
	private Calendar lastModified;

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Calendar getLastModified() {
		if (lastModified == null) {
			lastModified = Calendar.getInstance();
		}
		return lastModified;
	}

	public void setLastModified(Calendar lastModified) {
		this.lastModified = lastModified;
	}

	@Override
	protected void loadEager(Node node) {
		loadLazy(node);
		try {
			if (node.hasNode("jcr:content")) {
				Node content = node.getNode("jcr:content");
				if (content.hasProperty("jcr:data")) {
					Binary bin = content.getProperty("jcr:data").getBinary();
					if (bin != null) {
						setInputStream(content.getProperty("jcr:data").getBinary().getStream());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void loadLazy(Node node) {
		try {
			if (node.hasNode("jcr:content")) {
				Node content = node.getNode("jcr:content");
				setFilename(node.getName());
				if (content.hasProperty("jcr:mimeType")) {
					setMimeType(content.getProperty("jcr:mimeType").getString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected Node addToNode(Node parent, ValueFactory valueFactory) throws BusinessEntityException {
		Node result = null;
		try {
			result = parent.addNode(getFilename(), "nt:file");
			Node content = result.addNode("jcr:content", "nt:resource");
			content.setProperty("jcr:mimeType", getMimeType());
			Binary bin = valueFactory.createBinary(getInputStream());
			content.setProperty("jcr:data", bin);
			Calendar lastModified = Calendar.getInstance();
			content.setProperty("jcr:lastModified", lastModified);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	protected void updateNodeEager(ValueFactory valueFactory) throws BusinessEntityException {
		updateNodeLazy(valueFactory);

	}

	@Override
	protected void updateNodeLazy(ValueFactory valueFactory) throws BusinessEntityException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setId(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isTransient() {
		// TODO Auto-generated method stub
		return false;
	}

}
