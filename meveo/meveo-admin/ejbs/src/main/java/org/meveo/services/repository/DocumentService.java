package org.meveo.services.repository;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.jcr.ItemExistsException;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.nodetype.NoSuchNodeTypeException;
import javax.jcr.version.VersionException;

import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.service.base.BusinessEntityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import antlr.collections.List;

@Stateless
public class DocumentService extends JCRPersistenceService<Document>{

	Logger log = LoggerFactory.getLogger(this.getClass());
    
    public Node getRootNode() throws RepositoryException, javax.jcr.RepositoryException{
        Node result = null; 	
        result = getSession().getRootNode();
        log.info("getRootNode hasNode('accounts')="+result.hasNode("accounts"));
        if(!result.hasNode("accounts")){
           Node accountsNode = result.addNode("accounts", "nt:folder");
           log.info("added node 'accounts' : "+accountsNode.getPath());
           log.info("accounts hasNode('ADMI')="+accountsNode.hasNode("ADMI"));
           Node adminNode = accountsNode.addNode("ADMI", "nt:folder");
           log.info("added node 'ADMI' : "+adminNode.getPath());
           getSession().save();
           log.info("saved session");
        }        
        return result;
    }
    

    public void createAccountNode(String accountCode) throws RepositoryException,ItemExistsException, PathNotFoundException, NoSuchNodeTypeException, LockException, VersionException, ConstraintViolationException, javax.jcr.RepositoryException{
        Node accountsNode = getAccountsNode();
        accountsNode.addNode(accountCode,"nt:folder");
    }
    
    public Node getAccountsNode() throws RepositoryException, PathNotFoundException, javax.jcr.RepositoryException{
        log.info("getAccountsNode");
        return getRootNode().getNode("accounts");
    }
    
    public Node getAccountNode(String accountCode) throws RepositoryException, PathNotFoundException, javax.jcr.RepositoryException{
        log.info("getAccountNode("+accountCode+")");
        Node result=null;
        if(accountCode!=null && accountCode.length()>0){
            result=getAccountsNode().getNode(accountCode);
        }
        log.info("getAccountNode result :"+result==null?"null":result.getPath());
        return result;
    }
    
    public Node getDocumentsNode(String accountCode) throws RepositoryException, PathNotFoundException, javax.jcr.RepositoryException{
        log.info("getDocumentsNode("+accountCode+")");
        Node result=null;
        Node accountNode=getAccountNode(accountCode);
        if(accountNode!=null && !accountNode.hasNode("documents")){
            accountNode.addNode("documents", "nt:folder");
            getSession().save();
        }
        if(accountNode!=null){
            result=accountNode.getNode("documents");
        }
        log.info("getDocumentsNode result :"+result==null?"null":result.getPath());
        return result;
    }
    
        
        
    public void addDocument(String accountCode,Document document) throws BusinessException{
        log.info("addDocument("+accountCode+","+document==null?"null":document.getFilename()+")");
        try {
            create(document, getAccountNode(accountCode));
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessEntityException("Error getting Account node for code "+accountCode, e);
        }
    }
    
    

    public int count(String accountCode,String filename) {
        log.info("count("+accountCode+","+filename+")");
        int result=0;
        try {
            result = (int)getDocumentsNode(accountCode).getNodes(filename).getSize();
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("count result :"+result);
        return result;
    }

    public List find(String accountCode,String filename,PaginationConfiguration configuration) {
        List result = (List) new ArrayList<Document>();
        try {
            javax.jcr.NodeIterator nodes = getDocumentsNode(accountCode).getNodes(filename);
            if(nodes.getSize()>configuration.getFirstRow()){
                nodes.skip(configuration.getFirstRow());
            }
            ArrayList<Document> arrayList = (ArrayList<Document>) result;
			while (nodes.hasNext() && arrayList.size()<configuration.getNumberOfRows()) {
                Node node = nodes.nextNode();
                try {
                    if (node.hasNode("jcr:content")) {
                        Document document = new Document();
                        document.load(node, false);
                        result.add(document);
                        break;
                    }
                } catch (BusinessException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return result;
    }
    
}
