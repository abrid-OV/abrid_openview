package org.meveo.export;

import java.io.Serializable;

/**
 * @author Edward P. Legaspi
 * @since Oct 4, 2013
 **/
public abstract class BaseDto implements Serializable {

	private static final long serialVersionUID = 4456089256601996946L;

	public void validate() throws InvalidDTOException {

	}

}
