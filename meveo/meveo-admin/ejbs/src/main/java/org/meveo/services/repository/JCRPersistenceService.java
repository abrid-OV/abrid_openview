package org.meveo.services.repository;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jcr.LoginException;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;

import org.jboss.solder.logging.Logger;
import org.meveo.admin.exception.BusinessException;
import org.meveo.model.jcr.JCREntity;
import org.meveo.security.MeveoUser;
import org.meveo.service.base.BaseService;
import org.meveo.service.base.BusinessEntityException;

public class JCRPersistenceService<E extends JCREntity> extends BaseService {
    
    protected final Class<E> entityClass;
    
    @Resource(mappedName="java:/jcr/ecportal")
    private javax.jcr.Repository repository;
    
    SimpleCredentials cred = new SimpleCredentials("admin","admin".toCharArray());
    Session session;
    
    public Session getSession() throws LoginException, RepositoryException{
        if(session==null){
            log.info("create new repository session");
            session=repository.login(cred);
        }
        return session;
    }
 
    @Inject
    private Logger log;

    @SuppressWarnings("unchecked")
    public JCRPersistenceService() {

        Class clazz = getClass();
        while (!(clazz.getGenericSuperclass() instanceof ParameterizedType)) {
            clazz = clazz.getSuperclass();
        }
        Object o = ((ParameterizedType) clazz.getGenericSuperclass()).getActualTypeArguments()[0];

        if (o instanceof TypeVariable) {
            this.entityClass = (Class<E>) ((TypeVariable) o).getBounds()[0];
        } else {
            this.entityClass = (Class<E>) o;
        }
    }

    public Class<E> getEntityClass() {
        return entityClass;
    }

    /**
     * Method called before creating an entity, to perform business validation. The default implementation is empty, and should be overriden when it's needed.
     * 
     * @param e
     * @throws BusinessException
     */
    public void validateBeforeCreate(E e) throws BusinessException {

    }

    /**
     * Method called before updating an entity, to perform business validation. The default implementation is empty, and should be overriden when it's needed.
     * 
     * @param e
     * @throws BusinessException
     */
    public void validateBeforeUpdate(E e) throws BusinessException {

    }

    /**
     * Method called before deleting an entity, to perform business validation. The default implementation is empty, and should be overriden when it's needed.
     * 
     * @param e
     * @throws BusinessException
     */
    public void validateBeforeRemove(E e) throws BusinessException {

    }

    public E findById(String id) {
        return findById(id, false);
    }

    public E findById(String id, boolean refresh) {
        log.debugf("start of find {0} by id (id=#1) ..", getEntityClass().getSimpleName(), id);
        final Class<? extends E> productClass = getEntityClass();
        E e=null;
        try {
            Node node = getSession().getNodeByIdentifier(id);
            e = productClass.getDeclaredConstructor().newInstance();
            e.load(node, true);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        if (refresh) {
            log.debugf("refreshing JCR entity do nothing");
            //em.refresh(e);
        }
        log.debugf("end of find {0} by id (id=#1). Result found={2}.", getEntityClass().getSimpleName(), id, e != null);
        return e;
    }

    public void create(E e,Node node) throws BusinessException {
        log.info("create "+e+" node="+node);
        create(e, null,node);
    }

    public void update(E e) throws BusinessException {
        update(e, getMeveoUser());
    }

    public E remove(String id) throws BusinessException {
        E e = findById(id);
        if (e != null) {
            validateBeforeRemove(e);
            remove(e);
        }
        return e;
    }

    public void remove(E e) throws BusinessException {
        log.debugf("start of remove {0} entity (id=#1) ..", getEntityClass().getSimpleName(), e.getId());
        try {
            log.debugf("start of validation ..");
            validateBeforeRemove(e);
            log.debugf("validation OK.");
            e.remove();
            getSession().save();
            
        } catch (BusinessException ex) {
            log.errorf("validation KO. Cause : {0}.", ex.getMessage());
            throw ex;
        }   catch (Exception e1) {
            log.errorf("error while saving JCR session KO. Cause : {0}.", e1.getMessage());
            throw new BusinessEntityException("error while saving JCR session",e1);
        }
        log.debugf("end of remove {0} entity (id=#1).", getEntityClass().getSimpleName(), e.getId());
    }

    public void update(E e, MeveoUser updater) throws BusinessException {
        log.debugf("start of update {0} entity (id=#1) ..", e.getClass().getSimpleName(), e.getId());
        try {
            e.merge(getSession().getValueFactory());
            log.debugf("start of validation ..");
            validateBeforeUpdate(e);
            log.debugf("validation OK.");
            if (e instanceof JCRAuditableEntity) {
                if (updater != null) {
                    ((JCRAuditableEntity) e).updateAudit(updater.getUser());
                } else {
                    ((JCRAuditableEntity) e).updateAudit(getCurrentUser());
                }
            }
            
        } catch (BusinessException ex) {
            log.errorf("validation KO. Cause : {0}.", ex.getMessage());
            throw ex;
        } catch (Exception e1) {
            log.errorf("error while saving JCR session KO. Cause : {0}.", e1.getMessage());
            throw new BusinessEntityException("error while saving JCR session",e1);
        }
        log.debugf("end of update {0} entity (id=#1).", e.getClass().getSimpleName(), e.getId());
    }

    public void refresh(E e) {
        //nothing to do right now for JCR
        //em.refresh(e);
    };

    public void create(E e, MeveoUser creator, Node parent) throws BusinessException {
        log.debugf("start of create {0} entity ..", e.getClass().getSimpleName());
        try {
            log.debugf("start of validation ..");
            validateBeforeCreate(e);
            log.debugf("validation OK.");
            if (e instanceof JCRAuditableEntity) {
                if (creator != null) {
                    ((JCRAuditableEntity) e).updateAudit(creator.getUser());
                } else {
                    ((JCRAuditableEntity) e).updateAudit(getCurrentUser());
                }
            }
            e.persist(parent,getSession().getValueFactory());
        } catch (BusinessException ex) {
            log.errorf("validation KO. Cause : {0}.", ex.getMessage());
            throw ex;
        }catch (Exception e1) {
            log.errorf("error while getting JCR ValueFactory from session KO. Cause : {0}.", e1.getMessage());
            throw new BusinessEntityException("error while getting JCR ValueFactory from session ",e1);
        }
        log.debugf("end of create {0}. entity id=#1.", e.getClass().getSimpleName(), e.getId());
    };

    public List<String> listIds(Collection<E> eList) {
        List<String> results = new ArrayList<String>();
        if ((eList != null) && (!eList.isEmpty())) {
            for (E e : eList) {
                if ((e != null) && (e.getId() != null)) {
                    results.add((String)e.getId());
                }
            }
        }
        return results;
    }

}
