package org.meveo.model.jcr;

import java.io.Serializable;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFactory;

import org.meveo.model.IEntity;
import org.meveo.service.base.BusinessEntityException;


public abstract class JCREntity implements Serializable, IEntity, Cloneable {
   
    private static final long serialVersionUID = 699056148395169672L;
    
    private boolean lazyLoaded=true;
    protected Node node=null;
    
    protected abstract void loadEager(Node node);
    protected abstract void loadLazy(Node node);
    protected abstract Node addToNode(Node parent,ValueFactory valueFactory) throws BusinessEntityException;
    protected abstract void updateNodeEager(ValueFactory valueFactory) throws BusinessEntityException;
    protected abstract void updateNodeLazy(ValueFactory valueFactory) throws BusinessEntityException;
        
    public void load(Node node,boolean eager) throws BusinessEntityException{
        if(node!=null){
            String id=null;
            try {
                id=node.getIdentifier();
            } catch (RepositoryException e) {
                e.printStackTrace();
            }
            throw new BusinessEntityException("JCR entity already loaded", id);
        }
        lazyLoaded=!eager;
        if(lazyLoaded){
            loadLazy(node);
        } else {
            loadEager(node);
        }
        this.node=node;
    }
    
    public void persist(Node parent,ValueFactory valueFactory) throws BusinessEntityException{
        if(getId()!=null){
            throw new BusinessEntityException("JCR entity already persisted", (String)getId());            
        } else if(parent==null){
            throw new BusinessEntityException("JCR entity cannot be persisted to null parent");            
        } else {
            this.node = addToNode(parent,valueFactory);
        }
    }
    
    public void merge(ValueFactory valueFactory) throws BusinessEntityException{
        if(node==null){
            throw new BusinessEntityException("detached JCR entity cannot be merged as it need parent");            
        } else {
            if(lazyLoaded){
                updateNodeLazy(valueFactory);
            } else {
                updateNodeEager(valueFactory);
            }
        }
    }
    
    public void remove()  throws BusinessEntityException{
        if(node==null){
            throw new BusinessEntityException("detached JCR entity cannot be removed");            
        } else {
            try {
                node.remove();
            } catch (Exception e) {
                //e.printStackTrace();
                throw new BusinessEntityException("error while removing JCR entity",e);       
            }
        }
    }
    
    @Override
    public Serializable getId() {
        Serializable result=null;
        try {
            result = (node==null)?null:node.getIdentifier();
        } catch (RepositoryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof JCREntity))
            return false;
        final JCREntity other = (JCREntity) obj;
        if (getId() == null) {
            return false;
        } else if (!getId().equals(other.getId()))
            return false;
        return true;
    }
}
