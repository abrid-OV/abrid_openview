package org.meveo.documents;

import javax.inject.Named;

@Named
public enum CommentFieldsEnum {
    companyType, taxationRegime, accountingMethod, bkReconcLastReport, lastCA3, slips, borrowingAccounting, rentAccounting, purchesInvoices, saleInvoices, bankMouvment, assuranceAccounting, socialChargesAccounting, withdrawalsAccounting,

    caVentillation,

    fuelRecovery,

    carChargesRecovery,

    otherCharges;

}
