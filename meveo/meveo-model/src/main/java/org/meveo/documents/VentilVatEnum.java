package org.meveo.documents;

public enum VentilVatEnum {
	EXONERE(1,"VentilVatEnum.exonere"), REDUITE(2,"VentilVatEnum.reduit"), SEMIREDUIT(3,"VentilVatEnum.semireduit"), PLEINE(4,"VentilVatEnum.pleine");

    private String label;
    private Integer id;

    VentilVatEnum(Integer id, String label) {
        this.label = label;
        this.id = id;
    }

    public String getLabel() {
        return this.label;
    }

    public Integer getId() {
        return this.id;
    }
}
