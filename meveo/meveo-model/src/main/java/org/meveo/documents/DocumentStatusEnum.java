package org.meveo.documents;

public enum DocumentStatusEnum {
    UPLOADED("Scanné/Télechargé"), SENT("Transmis pour traitement"), ACKNOWLEDGED("En cours de traitement"), RECORDED("Traité comptablement"), REJECTED("Rejeté");

    private String label;

    private DocumentStatusEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
