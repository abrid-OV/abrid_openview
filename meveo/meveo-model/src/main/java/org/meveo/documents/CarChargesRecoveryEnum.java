package org.meveo.documents;

import javax.inject.Named;

@Named
public enum CarChargesRecoveryEnum {
    NO_RECOVERY(1, "carChargesRecoveryEnum.noRecovery"), TOTAL_RECOVERY(2, "carChargesRecoveryEnum.totalRecovery");

    private String label;
    private Integer id;

    CarChargesRecoveryEnum(Integer id, String label) {
        this.label = label;
        this.id = id;
    }

    public String getLabel() {
        return this.label;
    }

    public Integer getId() {
        return this.id;
    }

    public static CarChargesRecoveryEnum getValue(Integer id) {
        if (id != null) {
            for (CarChargesRecoveryEnum status : values()) {
                if (id.equals(status.getId())) {
                    return status;
                }
            }
        }
        return null;
    }
}
