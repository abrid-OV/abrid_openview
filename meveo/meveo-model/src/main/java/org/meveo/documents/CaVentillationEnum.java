package org.meveo.documents;

import javax.inject.Named;

@Named
public enum CaVentillationEnum {
    TVA_EXEMPT(1, "caVentillationEnum.tvaExempt"), TVA_REDUCED(2, "caVentillationEnum.tvaReduced"), TVA_SEMI_REDUCED(3, "caVentillationEnum.tvaSemiReduced"), TVA_COMPLET(4,
            "caVentillationEnum.tvaComplet");

    private String label;
    private Integer id;

    CaVentillationEnum(Integer id, String label) {
        this.label = label;
        this.id = id;
    }

    public String getLabel() {
        return this.label;
    }

    public Integer getId() {
        return this.id;
    }

    public static CaVentillationEnum getValue(Integer id) {
        if (id != null) {
            for (CaVentillationEnum status : values()) {
                if (id.equals(status.getId())) {
                    return status;
                }
            }
        }
        return null;
    }
}
