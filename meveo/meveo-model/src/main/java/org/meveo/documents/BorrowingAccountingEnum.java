package org.meveo.documents;

import javax.inject.Named;

@Named
public enum BorrowingAccountingEnum {
    CAPITAL_REIMBURSEMENT_ONLY(1, "borrowingAccountingEnum.capatalReimbursementOnly"), CAPITAL_REIMUBRS_INTEREST_PAYM(2, "borrowingAccountingEnum.capitalReibursAndInterestPayment");

    private String label;
    private Integer id;

    BorrowingAccountingEnum(Integer id, String label) {
        this.label = label;
        this.id = id;
    }

    public String getLabel() {
        return this.label;
    }

    public Integer getId() {
        return this.id;
    }

    public static BorrowingAccountingEnum getValue(Integer id) {
        if (id != null) {
            for (BorrowingAccountingEnum status : values()) {
                if (id.equals(status.getId())) {
                    return status;
                }
            }
        }
        return null;
    }
}
