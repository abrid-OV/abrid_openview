package org.meveo.documents;

public enum RecupVatEnum {
	NORECUP(1,"RecupVatEnum.norecup"), HIGHRECUP(2,"RecupVatEnum.highrecup"), FULLRECUP(3,"RecupVatEnum.fullrecup");

    private String label;
    private Integer id;

    RecupVatEnum(Integer id, String label) {
        this.label = label;
        this.id = id;
    }

    public String getLabel() {
        return this.label;
    }

    public Integer getId() {
        return this.id;
    }
}
