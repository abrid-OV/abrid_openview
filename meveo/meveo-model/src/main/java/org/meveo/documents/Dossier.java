package org.meveo.documents;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.meveo.model.BusinessEntity;
import org.meveo.model.admin.AccountEntity;
import org.meveo.model.admin.BusinessName;
import org.meveo.model.admin.User;

@Entity
@Table(name = "DOSSIER") 
@SequenceGenerator(name = "ID_GENERATOR", sequenceName = "DOSSIER_SEQ")
public class Dossier extends  BusinessEntity {

	 private static final long serialVersionUID = 1L;

	    @ManyToOne(fetch = FetchType.LAZY)
	    @JoinColumn(name = "EC_CONTACT_ID")
	    private User ecContact;

	    @Column(name = "EC_RESPONSIBLE_EMAIL", length = 50)
	    private String ecResponsibleEmail;

	    @Column(name = "EC_RESPONSIBLE_TEL", length = 20)
	    private String ecResponsibleTel;

	    @Column(name = "STATUS", length = 20)
	    @Enumerated(EnumType.STRING)
	    private DossierStatusEnum status;

	    @Column(name = "EC_RESPONSIBLE_SKYPE", length = 20)
	    private String ecResponsibleSkype;
	    
	    @Column(name = "ACTIVITY_CODE", length = 10)
	    private String activityCode;

	    @Column(name = "ACTIVITY_DESCRIPTION", length = 255)
	    private String activityDescription;

	    @Column(name = "ENGAGEMENT", length = 255)
	    private String engagement;
	    
	    @Column(name = "TRESORERIE", length = 255)
	    private String tresorerie;
	    
	    @Enumerated(EnumType.STRING)
	    @Column(name = "VAT_Mode", length = 20)
	    private VATModeEnum vatMode = VATModeEnum.EXEMPT;

	    @Temporal(TemporalType.TIMESTAMP)
		@Column(name = "VAT_FILING_DATE", length = 12)
	    private Date vatFilingDate;

	    @Enumerated(EnumType.STRING)
	    @Column(name = "TAXATION_REGIME", length = 10)
	    private TaxationRegimeEnum taxationRegime = TaxationRegimeEnum.BNC;
	    
	    @Enumerated(EnumType.STRING)
	    @Column(name = "FORME_JURIDIC", length = 10)
	    private JuridicEnum formeJuridic = JuridicEnum.SA;
	    
	    @Enumerated(EnumType.STRING)
	    @Column(name = "VAT_VENTILATION", length = 50)
	    private VentilVatEnum ventilation = VentilVatEnum.EXONERE;
	    
	    @Enumerated(EnumType.STRING)
	    @Column(name = "VAT_RECUPERE", length = 50)
	    private RecupVatEnum recupere = RecupVatEnum.NORECUP;
	    
	    public VentilVatEnum getVentilation() {
			return ventilation;
		}

		public void setVentilation(VentilVatEnum ventilation) {
			this.ventilation = ventilation;
		}

		public String getEngagement() {
			return engagement;
		}

		public void setEngagement(String engagement) {
			this.engagement = engagement;
		}

		public String getTresorerie() {
			return tresorerie;
		}

		public void setTresorerie(String tresorerie) {
			this.tresorerie = tresorerie;
		}

		public JuridicEnum getFormeJuridic() {
			return formeJuridic;
		}

		public void setFormeJuridic(JuridicEnum formeJuridic) {
			this.formeJuridic = formeJuridic;
		}

		public String getDossierComment() {
			return dossierComment;
		}

		public void setDossierComment(String dossierComment) {
			this.dossierComment = dossierComment;
		}

		public String getActivityComment() {
			return activityComment;
		}

		public void setActivityComment(String activityComment) {
			this.activityComment = activityComment;
		}

		public String getActivityRegimeComment() {
			return activityRegimeComment;
		}

		public void setActivityRegimeComment(String activityRegimeComment) {
			this.activityRegimeComment = activityRegimeComment;
		}

		public String getRapproche() {
			return rapproche;
		}

		public void setRapproche(String rapproche) {
			this.rapproche = rapproche;
		}

		public String getActivityCa3() {
			return activityCa3;
		}

		public void setActivityCa3(String activityCa3) {
			this.activityCa3 = activityCa3;
		}

		public String getBorderu() {
			return borderu;
		}

		public void setBorderu(String borderu) {
			this.borderu = borderu;
		}

		public String getActivityP1Comment() {
			return activityP1Comment;
		}

		public void setActivityP1Comment(String activityP1Comment) {
			this.activityP1Comment = activityP1Comment;
		}

		public String getActivityP2Comment() {
			return activityP2Comment;
		}

		public void setActivityP2Comment(String activityP2Comment) {
			this.activityP2Comment = activityP2Comment;
		}

		public String getActivityP3Comment() {
			return activityP3Comment;
		}

		public void setActivityP3Comment(String activityP3Comment) {
			this.activityP3Comment = activityP3Comment;
		}
		
		public static long getSerialversionuid() {
			return serialVersionUID;
		}
	    
	    @Column(name = "INCOME_TAX_MODE", length = 10)
	    private String IncomeTaxMode;

	    @Temporal(TemporalType.TIMESTAMP)
	    @Column(name = "STARTING_DATE")
	    private Date startingDate;

	    @Temporal(TemporalType.TIMESTAMP)
	    @Column(name = "ENDING_DATE")
	    private Date endingDate;

	    @Column(name = "REMINDER_DATE", length = 12)
	    private Integer reminderDate;

	    @Temporal(TemporalType.TIMESTAMP)
	    @Column(name = "LAST_REMINDER_DATE")
	    private Date lastReminderDate;

	    @Column(name = "ALERT_DATE", length = 2)
	    private Integer alertDate;

	    @Temporal(TemporalType.TIMESTAMP)
	    @Column(name = "LAST_ALERT_DATE")
	    private Date lastAlertDate;

	    @Column(name = "PERIOD_TYPE", length = 6)
	    private PeriodTypeEnum periodType;

	    @Column(name = "RECORD_NUM", length = 10)
	    private String recordNum;

	    @Column(name = "RECORD_NAME", length = 50)
	    private String recordName;

	    @Column(name = "PURCHASE_INVOICES")
	    private boolean purchesInvoices;

	    @Column(name = "SALE_INVOICES")
	    private boolean saleInvoices;

	    @Column(name = "BANK_MOUVMENT")
	    private boolean bankMouvment;

	    @Column(name = "BANK_RECONC_STATEMENT")
	    private boolean bankReconcStatement;

	    @Enumerated(EnumType.STRING)
	    @Column(name = "BORROWING_ACCOUNTING")
	    private BorrowingAccountingEnum borrowingAccounting = BorrowingAccountingEnum.CAPITAL_REIMBURSEMENT_ONLY;

	    @Enumerated(EnumType.STRING)
	    @Column(name = "RENT_ACCOUNTING")
	    private RentAccountingEnum rentAccounting = RentAccountingEnum.DUE_NOTICE;

	    @Enumerated(EnumType.STRING)
	    @Column(name = "ACCOUNTING_METHOD")
	    private AccountingMethodEnum accountingMethod;

	    @Column(name = "BANK_RECONC_LAST_REPORT")
	    private boolean banckReconcLastReport;

	    @Column(name = "LAST_CA3")
	    private boolean lastCA3;

	    @Column(name = "SLIPS")
	    private boolean slips;

	    @Enumerated(EnumType.STRING)
	    @Column(name = "ASSURANCE_ACCOUNTING")
	    private AssuranceAccountingEnum assuranceAccounting = AssuranceAccountingEnum.RECIEPT;

	    @Enumerated(EnumType.STRING)
	    @Column(name = "SOCIAL_CHARGES_ACCOUNTING")
	    private SocialChargesAccountingEnum socialChargesAccounting = SocialChargesAccountingEnum.CALL_SLIPS_CONTRIBUTION;

	    @Enumerated(EnumType.STRING)
	    @Column(name = "WITHDRAWLS_ACCOUNTING")
	    private WithdrawalsAccountingEnum withdrawalsAccounting = WithdrawalsAccountingEnum.ACCOUNT_180_455;

	    @Enumerated(EnumType.STRING)
	    @Column(name = "CA_VENTILLATION")
	    private CaVentillationEnum caVentillation = CaVentillationEnum.TVA_EXEMPT;

	    @Enumerated(EnumType.STRING)
	    @Column(name = "FUEL_RECOVERY")
	    private FuelRecoveryEnum fuelRecovery;

	    @Enumerated(EnumType.STRING)
	    @Column(name = "CAR_CHARGES_RECOVERY")
	    private CarChargesRecoveryEnum carChargesRecoveryEnum;

	    @Column(name = "OTHER_CHARGES")
	    private Boolean otherCharges;

	    @Column(name = "SPECIFIC_ACCOUNTING_ENTRIES", length = 255)
	    private String specificAccountingEntries;

	    @Column(name = "REMINDER_DELAY", length = 6)
	    private Integer reminderDelay;

	    @Column(name = "ALERT_DELAY", length = 6)
	    private Integer alertDelay;

	 // DOSSIER COLS
	    
	    @Column(name = "INTERLOCUTOR_NAME", length = 50)
	    private String interlocutorName;

	    @Column(name = "INTERLOCUTOR_EMAIL", length = 50)
	    private String interlocutorEmail;
	    
	    @Column(name = "DOSSIER_COMMENT", length = 500)
	    private String dossierComment;
	   
	    @Column(name = "ACTIVITY_COMMENT", length = 500)
	    private String activityComment;
	    
	    @Column(name = "ACTIVITY_REGIME_COMMENT", length = 500)
	    private String activityRegimeComment;
	    
	    @Column(name = "RAPPROCHE", length = 100)
	    private String rapproche;
	    
	    @Column(name = "ACTIVITY_CA3", length = 100)
	    private String activityCa3;
	    
	    @Column(name = "BORDEREAU", length = 100)
	    private String borderu;
	    
	    @Column(name = "ACTIVITY_P1COMMENT", length = 500)
	    private String activityP1Comment;
	    
	    @Column(name = "ACTIVITY_P2COMMENT", length = 500)
	    private String activityP2Comment;
	    
	    @Column(name = "ACTIVITY_P3COMMENT", length = 500)
	    private String activityP3Comment;
	    
	    @Column(name = "VENTILATION_COMMENT", length = 500)
	    private String ventilComment;
	    
	    @Column(name = "VAT_COMMENT", length = 500)
	    private String VATComment;
	    
	    @Column(name = "RECUP_CARBURANT", length = 100)
	    private String carburant;
	    
	    @Column(name = "RECUP_CHARGE", length = 100)
	    private String charges;
	    
	    @Column(name = "RECUP_IMPOTS", length = 100)
	    private String autreimpo;
	    
	    // DOCUMENT COLS
	    
	    @Column(name = "DEPOT_CODE_DOSSIER", length = 100)
	    private Integer depocodeDos;
	    
	    @Column(name = "DEPOT_NOM_DOSSIER", length = 100)
	    private String deponomDos;
	    
	    @Column(name = "DEPOT_PERIOD", length = 100)
	    private Date depoPeriod;
	    
	    @Column(name = "DEPOT_JOURNAL", length = 100)
	    private String depoJrnl;
	    
	    @Column(name = "DEPOT_NOM_FICHIER", length = 100)
	    private String depNomfich;
	    
	    @Column(name = "PIECE_NOM_DOSSIER", length = 100)
	    private String docName;
	    
	    @Column(name = "PIECE_STATUT", length = 100)
	    private String piecStatut;
	    
	    @Column(name = "PIECE_PERIOD_COMPT", length = 100)
	    private String piecPeriodecompt;
	    
	    @Column(name = "PIECE_TYPE", length = 100)
	    private String piecDocType;
	    
	    @Column(name = "PIECE_LIBELLE", length = 100)
	    private String piecLibel;

	    @Column(name = "PIECE_DATE_PIECE", length = 100)
	    private String piecDatePiece;
	    
	    @Column(name = "PIECE_nFACTURE", length = 100)
	    private String nFacture;
	    
	    @Column(name = "PIECE_CODE_DOSSIER", length = 100)
	    private String docCode;
	    
	    @Column(name = "PIECE_JOURNAL", length = 100)
	    private String piecJournal;
	    
	    @Column(name = "PIECE_PERIOD_TRAIT", length = 100)
	    private String piecPeriodTrait;
	    
	    @Column(name = "PIECE_TIERS", length = 100)
	    private String piecTiers;
	    
	    @Column(name = "PIECE_refGED", length = 100)
	    private String piecRef;
	    
	    @Column(name = "PIECE_TTC", length = 100)
	    private String piecMontTT;
	    
	    @Column(name = "PIECE_perCOMPT", length = 100)
	    private String PiecCompt;
	    
	    @Column(name = "CONS_CODE_DOSSIER", length = 100)
	    private String cDocId;
	    
	    @Column(name = "CONS_STATUT", length = 100)
	    private String conStatut;
	    
	    @Column(name = "CONS_PERIOD_COMPT", length = 100)
	    private String cPeriodecompt;
	    
	    @Column(name = "CONS_TYPE", length = 100)
	    private String conType;
	    
	    @Column(name = "CONS_NOM_DOSSIER", length = 100)
	    private String cDocName;
	    
	    @Column(name = "CONS_JOURNAL", length = 100)
	    private String cJournal;
	   
	    @Column(name = "CONS_PERIOD_TRAIT", length = 100)
	    private String cPeriodTrait;
	    
	    @Column(name = "CONS_NOM_FICHIER", length = 100)
	    private String cNumFact;
	    
	    @ManyToOne(fetch = FetchType.LAZY)
		@JoinColumn(name = "ACCOUNT_ENTITY_ID")
		private AccountEntity accountEntity;

	    private BusinessName businessName = new BusinessName();

	    public String getActivityCode() {
	        return activityCode;
	    }

	    public void setActivityCode(String activityCode) {
	        this.activityCode = activityCode;
	    }

	    public String getActivityDescription() {
	        return activityDescription;
	    }

	    public void setActivityDescription(String activityDescription) {
	        this.activityDescription = activityDescription;
	    }

	    public VATModeEnum getVatMode() {
	        return vatMode;
	    }

	    public void setVatMode(VATModeEnum vatMode) {
	        this.vatMode = vatMode;
	    }

	    public String getIncomeTaxMode() {
	        return IncomeTaxMode;
	    }

	    public void setIncomeTaxMode(String incomeTaxMode) {
	        IncomeTaxMode = incomeTaxMode;
	    }

	    public Date getStartingDate() {
	        return startingDate;
	    }

	    public void setStartingDate(Date startingDate) {
	        this.startingDate = startingDate;
	    }

	    public Integer getReminderDate() {
	        return reminderDate;
	    }

	    public void setReminderDate(Integer reminderDate) {
	        this.reminderDate = reminderDate;
	    }

	    public Date getLastReminderDate() {
	        return lastReminderDate;
	    }

	    public void setLastReminderDate(Date lastReminderDate) {
	        this.lastReminderDate = lastReminderDate;
	    }

	    public Integer getAlertDate() {
	        return alertDate;
	    }

	    public void setAlertDate(Integer alertDate) {
	        this.alertDate = alertDate;
	    }

	    public Date getLastAlertDate() {
	        return lastAlertDate;
	    }

	    public void setLastAlertDate(Date lastAlertDate) {
	        this.lastAlertDate = lastAlertDate;
	    }

	    public PeriodTypeEnum getPeriodType() {
	        return periodType;
	    }

	    public void setPeriodType(PeriodTypeEnum periodType) {
	        this.periodType = periodType;
	    }

	    public String getRecordNum() {
	        return recordNum;
	    }

	    public void setRecordNum(String recordNum) {
	        this.recordNum = recordNum;
	    }

	    public String getRecordName() {
	        return recordName;
	    }

	    public void setRecordName(String recordName) {
	        this.recordName = recordName;
	    }

	    public User getEcContact() {
	        return ecContact;
	    }

	    public void setEcContact(User ecContact) {
	        this.ecContact = ecContact;
	    }

	    public String getEcResponsibleEmail() {
	        return ecResponsibleEmail;
	    }

	    public void setEcResponsibleEmail(String ecResponsibleEmail) {
	        this.ecResponsibleEmail = ecResponsibleEmail;
	    }

	    public String getEcResponsibleTel() {
	        return ecResponsibleTel;
	    }

	    public void setEcResponsibleTel(String ecResponsibleTel) {
	        this.ecResponsibleTel = ecResponsibleTel;
	    }

	    public String getEcResponsibleSkype() {
	        return ecResponsibleSkype;
	    }

	    public void setEcResponsibleSkype(String ecResponsibleSkype) {
	        this.ecResponsibleSkype = ecResponsibleSkype;
	    }

	    public Date getEndingDate() {
	        return endingDate;
	    }

	    public void setEndingDate(Date endingDate) {
	        this.endingDate = endingDate;
	    }

	

	    public Date getVatFilingDate() {
			return vatFilingDate;
		}

		public void setVatFilingDate(Date vatFilingDate) {
			this.vatFilingDate = vatFilingDate;
		}

		public TaxationRegimeEnum getTaxationRegime() {
	        return taxationRegime;
	    }

	    public void setTaxationRegime(TaxationRegimeEnum taxationRegime) {
	        this.taxationRegime = taxationRegime;
	    }

	    public boolean isPurchesInvoices() {
	        return purchesInvoices;
	    }

	    public void setPurchesInvoices(boolean purchesInvoices) {
	        this.purchesInvoices = purchesInvoices;
	    }

	    public boolean isSaleInvoices() {
	        return saleInvoices;
	    }

	    public void setSaleInvoices(boolean saleInvoices) {
	        this.saleInvoices = saleInvoices;
	    }

	    public boolean isBankMouvment() {
	        return bankMouvment;
	    }

	    public void setBankMouvment(boolean bankMouvment) {
	        this.bankMouvment = bankMouvment;
	    }

	    public boolean isBankReconcStatement() {
	        return bankReconcStatement;
	    }

	    public void setBankReconcStatement(boolean bankReconcStatement) {
	        this.bankReconcStatement = bankReconcStatement;
	    }

	    public BorrowingAccountingEnum getBorrowingAccounting() {
	        return borrowingAccounting;
	    }

	    public void setBorrowingAccounting(BorrowingAccountingEnum borrowingAccounting) {
	        this.borrowingAccounting = borrowingAccounting;
	    }

	    public RentAccountingEnum getRentAccounting() {
	        return rentAccounting;
	    }

	    public void setRentAccounting(RentAccountingEnum rentAccounting) {
	        this.rentAccounting = rentAccounting;
	    }

	    public AccountingMethodEnum getAccountingMethod() {
	        return accountingMethod;
	    }

	    public void setAccountingMethod(AccountingMethodEnum accountingMethod) {
	        this.accountingMethod = accountingMethod;
	    }

	    public boolean isBanckReconcLastReport() {
	        return banckReconcLastReport;
	    }

	    public void setBanckReconcLastReport(boolean banckReconcLastReport) {
	        this.banckReconcLastReport = banckReconcLastReport;
	    }

	    public boolean isLastCA3() {
	        return lastCA3;
	    }

	    public void setLastCA3(boolean lastCA3) {
	        this.lastCA3 = lastCA3;
	    }

	    public boolean isSlips() {
	        return slips;
	    }

	    public void setSlips(boolean slips) {
	        this.slips = slips;
	    }

	    public AssuranceAccountingEnum getAssuranceAccounting() {
	        return assuranceAccounting;
	    }

	    public void setAssuranceAccounting(AssuranceAccountingEnum assuranceAccounting) {
	        this.assuranceAccounting = assuranceAccounting;
	    }

	    public SocialChargesAccountingEnum getSocialChargesAccounting() {
	        return socialChargesAccounting;
	    }

	    public void setSocialChargesAccounting(SocialChargesAccountingEnum socialChargesAccounting) {
	        this.socialChargesAccounting = socialChargesAccounting;
	    }

	    public WithdrawalsAccountingEnum getWithdrawalsAccounting() {
	        return withdrawalsAccounting;
	    }

	    public void setWithdrawalsAccounting(WithdrawalsAccountingEnum withdrawalsAccounting) {
	        this.withdrawalsAccounting = withdrawalsAccounting;
	    }

	    public Boolean getOtherCharges() {
	        return otherCharges;
	    }

	    public void setOtherCharges(Boolean otherCharges) {
	        this.otherCharges = otherCharges;
	    }

	    public String getSpecificAccountingEntries() {
	        return specificAccountingEntries;
	    }

	    public void setSpecificAccountingEntries(String specificAccountingEntries) {
	        this.specificAccountingEntries = specificAccountingEntries;
	    }

	    public CaVentillationEnum getCaVentillation() {
	        return caVentillation;
	    }

	    public void setCaVentillation(CaVentillationEnum caVentillation) {
	        this.caVentillation = caVentillation;
	    }

	    public FuelRecoveryEnum getFuelRecovery() {
	        return fuelRecovery;
	    }

	    public void setFuelRecovery(FuelRecoveryEnum fuelRecovery) {
	        this.fuelRecovery = fuelRecovery;
	    }

	    public CarChargesRecoveryEnum getCarChargesRecoveryEnum() {
	        return carChargesRecoveryEnum;
	    }

	    public void setCarChargesRecoveryEnum(CarChargesRecoveryEnum carChargesRecoveryEnum) {
	        this.carChargesRecoveryEnum = carChargesRecoveryEnum;
	    }

	    public Integer getReminderDelay() {
	        return reminderDelay;
	    }

	    public void setReminderDelay(Integer reminderDelay) {
	        this.reminderDelay = reminderDelay;
	    }

	    public Integer getAlertDelay() {
	        return alertDelay;
	    }

	    public void setAlertDelay(Integer alertDelay) {
	        this.alertDelay = alertDelay;
	    }


		public BusinessName getBusinessName() {
	        return businessName;
	    }

	    public void setBusinessName(BusinessName businessName) {
	        this.businessName = businessName;
	    }

	    public String getInterlocutorName() {
	        return interlocutorName;
	    }

	    public void setInterlocutorName(String interlocutorName) {
	        this.interlocutorName = interlocutorName;
	    }

	    public String getInterlocutorEmail() {
	        return interlocutorEmail;
	    }

	    public void setInterlocutorEmail(String interlocutorEmail) {
	        this.interlocutorEmail = interlocutorEmail;
	    }

	    public DossierStatusEnum getStatus() {
	        return status;
	    }

	    public void setStatus(DossierStatusEnum status) {
	        this.status = status;
	    }

		public AccountEntity getAccountEntity() {
			return accountEntity;
		}

		public void setAccountEntity(AccountEntity accountEntity) {
			this.accountEntity = accountEntity;
		}


	    
}
