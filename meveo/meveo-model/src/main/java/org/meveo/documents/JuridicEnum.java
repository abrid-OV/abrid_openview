package org.meveo.documents;

import javax.inject.Named;

@Named
public enum JuridicEnum {
	SA(1,"JuridicEnum.sa"), SARL(2,"JuridicEnum.sarl"), SNC(3,"JuridicEnum.snc"), SCS(4,"JuridicEnum.scs"),SCA(5,"JuridicEnum.sca");

    private String label;
    private Integer id;

    JuridicEnum(Integer id, String label) {
        this.label = label;
        this.id = id;
    }

    public String getLabel() {
        return this.label;
    }

    public Integer getId() {
        return this.id;
    }
}
