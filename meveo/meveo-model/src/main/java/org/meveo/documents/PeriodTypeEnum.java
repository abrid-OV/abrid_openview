package org.meveo.documents;

import javax.inject.Named;

@Named
public enum PeriodTypeEnum {
    MON(1, "periodTypeEnum.monthly"), TRI(2, "periodTypeEnum.quarterly"), AN(2, "periodTypeEnum.yearly");

    private String label;
    private Integer id;

    PeriodTypeEnum(Integer id, String label) {
        this.label = label;
        this.id = id;
    }

    public String getLabel() {
        return this.label;
    }

    public Integer getId() {
        return this.id;
    }

    public PeriodTypeEnum getValue(Integer id) {
        if (id != null) {
            for (PeriodTypeEnum status : values()) {
                if (id.equals(status.getId())) {
                    return status;
                }
            }
        }
        return null;
    }

}
