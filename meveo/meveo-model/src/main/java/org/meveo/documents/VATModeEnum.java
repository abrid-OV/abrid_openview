package org.meveo.documents;

import javax.inject.Named;

@Named
public enum VATModeEnum {
    EXEMPT(1, "vatModeNum.exempt"), SIMPLIFIED(2, "vatModeNum.simplified"), QUARTERLY(3, "vatModeNum.quarterly"), NORMAL(4, "vatModeNum.normal");

    private String label;
    private Integer id;

    VATModeEnum(Integer id, String label) {
        this.label = label;
        this.id = id;
    }

    public String getLabel() {
        return this.label;
    }

    public Integer getId() {
        return this.id;
    }

    public VATModeEnum getValue(Integer id) {
        if (id != null) {
            for (VATModeEnum status : values()) {
                if (id.equals(status.getId())) {
                    return status;
                }
            }
        }
        return null;
    }
}
