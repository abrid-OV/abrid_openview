package org.meveo.documents;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.meveo.model.AuditableEntity;

@Entity()
@Table(name = "Document")
@SequenceGenerator(name = "ID_GENERATOR", sequenceName = "DOCUMENT_SEQ")
public class Document extends AuditableEntity {

    private static final long serialVersionUID = -503081163920441844L;
    
    
    @Column(name = "name", length = 100, unique = true)
    private String name; 
   
    @Column(name = "originalFilename", length = 100)
    private String originalFilename; 
    
    @Column(name = "journal")
    private String journal; 
    
    @Column(name = "period")
    private String period;
   
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "insertionDate", length = 12)
    private Date insertionDate; 
    
    @Column(name = "isScan")
    private boolean isScan;
    
    @Column(name = "gedReference", length = 100)
    private String gedReference;  
    
    @OneToMany(mappedBy = "document", fetch = FetchType.LAZY)
	private List<Piece> pieces = new ArrayList<Piece>();
    
    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DOSSIER_ID")
	private Dossier dossier;
    
    @Enumerated(value = EnumType.STRING)
    @Column(name = "status")
    private DocumentStatusEnum documentStatus; 
    
    @Column(name = "rejectionReason")
    private String rejectionReason; 
    
    @Column(name = "processingPeriod")
    private String processingPeriod; 

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJournal() {
		return journal;
	}

	public void setJournal(String journal) {
		this.journal = journal;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public Date getInsertionDate() {
		return insertionDate;
	}

	public void setInsertionDate(Date insertionDate) {
		this.insertionDate = insertionDate;
	}

	public boolean isScan() {
		return isScan;
	}

	public void setScan(boolean isScan) {
		this.isScan = isScan;
	}

	public String getGedReference() {
		return gedReference;
	}

	public void setGedReference(String gedReference) {
		this.gedReference = gedReference;
	}

	public List<Piece> getPieces() {
		return pieces;
	}

	public void setPieces(List<Piece> pieces) {
		this.pieces = pieces;
	}

	public Dossier getDossier() {
		return dossier;
	}

	public void setDossier(Dossier dossier) {
		this.dossier = dossier;
	}

	public String getOriginalFilename() {
		return originalFilename;
	}

	public void setOriginalFilename(String originalFilename) {
		this.originalFilename = originalFilename;
	}

	public DocumentStatusEnum getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(DocumentStatusEnum documentStatus) {
		this.documentStatus = documentStatus;
	}

	public String getRejectionReason() {
		return rejectionReason;
	}

	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}

	public String getProcessingPeriod() {
		return processingPeriod;
	}

	public void setProcessingPeriod(String processingPeriod) {
		this.processingPeriod = processingPeriod;
	}
	
	

}