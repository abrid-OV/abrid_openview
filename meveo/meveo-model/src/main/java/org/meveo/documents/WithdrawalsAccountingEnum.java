package org.meveo.documents;

import javax.inject.Named;

@Named
public enum WithdrawalsAccountingEnum {
    ACCOUNT_180_455(1, "withdrawalsAccountingEnum.account_180_455"), REMUNERATION_ACCOUNT(2, "withdrawalsAccountingEnum.remunerationAccount");

    private String label;
    private Integer id;

    WithdrawalsAccountingEnum(Integer id, String label) {
        this.label = label;
        this.id = id;
    }

    public String getLabel() {
        return this.label;
    }

    public Integer getId() {
        return this.id;
    }

    public static WithdrawalsAccountingEnum getValue(Integer id) {
        if (id != null) {
            for (WithdrawalsAccountingEnum status : values()) {
                if (id.equals(status.getId())) {
                    return status;
                }
            }
        }
        return null;
    }
}
