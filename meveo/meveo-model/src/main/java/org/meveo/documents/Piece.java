package org.meveo.documents;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.meveo.model.AuditableEntity;

@Entity()
@Table(name = "PIECE")
@SequenceGenerator(name = "ID_GENERATOR", sequenceName = "PIECE_SEQ")
public class Piece extends AuditableEntity {

    private static final long serialVersionUID = -503081163920441944L;
    
    
    @Column(name = "name", length = 100, unique = true)
    private String name;

    @Column(name = "journal", length = 100)
    private String journal;

    @Column(name = "EMAIL", length = 100)
    private String email;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "insertion_date", length = 100)
    private Date insertionDate;
    
    @Column(name = "invoice_type", length = 100)
    private String invoiceType;
    
    @Column(name = "gedReference", length = 100)
    private String gedReference;
    
    
    @Column(name = "invoiceNum", length = 100)
    private String invoiceNum;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "INVOICE_DATE", length = 12)
    private Date invoiceDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DUE_DATE", length = 12)
    private Date dueDate;
    
    @Column(name = "invoiceDescription", length = 100)
    private String invoiceDescription;
    
    @Column(name = "amountWithTax", precision = 23, scale = 12)
	private BigDecimal amountWithTax;
    
    @Column(name = "amountWithoutTax", precision = 23, scale = 12)
	private BigDecimal amountWithoutTax;
    
    @Column(name = "currency", length = 10)
    private String currency;
    
    @Column(name = "thirdPartyName", length = 100)
    private String thirdPartyName;
    
    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DOCUMENT_ID")
	private Document document;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJournal() {
		return journal;
	}

	public void setJournal(String journal) {
		this.journal = journal;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getInsertionDate() {
		return insertionDate;
	}

	public void setInsertionDate(Date insertionDate) {
		this.insertionDate = insertionDate;
	}

	public String getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getGedReference() {
		return gedReference;
	}

	public void setGedReference(String gedReference) {
		this.gedReference = gedReference;
	}

	public String getInvoiceNum() {
		return invoiceNum;
	}

	public void setInvoiceNum(String invoiceNum) {
		this.invoiceNum = invoiceNum;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public String getInvoiceDescription() {
		return invoiceDescription;
	}

	public void setInvoiceDescription(String invoiceDescription) {
		this.invoiceDescription = invoiceDescription;
	}

	public BigDecimal getAmountWithTax() {
		return amountWithTax;
	}

	public void setAmountWithTax(BigDecimal amountWithTax) {
		this.amountWithTax = amountWithTax;
	}

	public BigDecimal getAmountWithoutTax() {
		return amountWithoutTax;
	}

	public void setAmountWithoutTax(BigDecimal amountWithoutTax) {
		this.amountWithoutTax = amountWithoutTax;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getThirdPartyName() {
		return thirdPartyName;
	}

	public void setThirdPartyName(String thirdPartyName) {
		this.thirdPartyName = thirdPartyName;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	
    
    
    
    
    
    
    
    
    
    
    
    


	
    
}
