package org.meveo.documents;

import javax.inject.Named;

@Named
public enum AccountingMethodEnum {
    COMMITMENT(1, "accountingMethodEnum.commitment"), TREASURY(2, "accountingMethodEnum.treasury");

    private String label;
    private Integer id;

    AccountingMethodEnum(Integer id, String label) {
        this.label = label;
        this.id = id;
    }

    public String getLabel() {
        return this.label;
    }

    public Integer getId() {
        return this.id;
    }

    public AccountingMethodEnum getValue(Integer id) {
        if (id != null) {
            for (AccountingMethodEnum status : values()) {
                if (id.equals(status.getId())) {
                    return status;
                }
            }
        }
        return null;
    }

}
