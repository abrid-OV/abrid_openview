package org.meveo.documents;

import javax.inject.Named;

@Named
public enum RentAccountingEnum {
    DUE_NOTICE(1, "rentAccountingEnum.dueNotice"), PAYEMENT_PROOF(4, "rentAccountingEnum.paymentProof");

    private String label;
    private Integer id;

    RentAccountingEnum(Integer id, String label) {
        this.label = label;
        this.id = id;
    }

    public String getLabel() {
        return this.label;
    }

    public Integer getId() {
        return this.id;
    }

    public static RentAccountingEnum getValue(Integer id) {
        if (id != null) {
            for (RentAccountingEnum status : values()) {
                if (id.equals(status.getId())) {
                    return status;
                }
            }
        }
        return null;
    }
}
