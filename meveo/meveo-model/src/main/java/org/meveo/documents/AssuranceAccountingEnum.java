package org.meveo.documents;

import javax.inject.Named;

@Named
public enum AssuranceAccountingEnum {
    RECIEPT(1, "AssuranceAccountingEnum.reciept"), PAYEMENT_PROOF(2, "AssuranceAccountingEnum.paymentProof");

    private String label;
    private Integer id;

    AssuranceAccountingEnum(Integer id, String label) {
        this.label = label;
        this.id = id;
    }

    public String getLabel() {
        return this.label;
    }

    public Integer getId() {
        return this.id;
    }

    public static AssuranceAccountingEnum getValue(Integer id) {
        if (id != null) {
            for (AssuranceAccountingEnum status : values()) {
                if (id.equals(status.getId())) {
                    return status;
                }
            }
        }
        return null;
    }
}
