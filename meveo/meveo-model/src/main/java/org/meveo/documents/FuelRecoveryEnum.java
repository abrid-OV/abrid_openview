package org.meveo.documents;

import javax.inject.Named;

@Named
public enum FuelRecoveryEnum {
    NO_RECOVERY(1, "fuelRecoveryEnum.noRecovery"), PARTIEL_RECOVERY(2, "fuelRecoveryEnum.partielRecovery"), TOTAL_RECOVERY(3, "fuelRecoveryEnum.totalRecovery");

    private String label;
    private Integer id;

    FuelRecoveryEnum(Integer id, String label) {
        this.label = label;
        this.id = id;
    }

    public String getLabel() {
        return this.label;
    }

    public Integer getId() {
        return this.id;
    }

    public static FuelRecoveryEnum getValue(Integer id) {
        if (id != null) {
            for (FuelRecoveryEnum status : values()) {
                if (id.equals(status.getId())) {
                    return status;
                }
            }
        }
        return null;
    }
}
