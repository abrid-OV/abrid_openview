package org.meveo.documents;

import javax.inject.Named;

@Named
public enum TaxationRegimeEnum {
    BNC(1, "taxationRegimeEnum.bnc"), IR(2, "taxationRegimeEnum.ir"), IS(3, "taxationRegimeEnum.is");

    private String label;
    private Integer id;

    TaxationRegimeEnum(Integer id, String label) {
        this.label = label;
        this.id = id;
    }

    public String getLabel() {
        return this.label;
    }

    public Integer getId() {
        return this.id;
    }

    public static TaxationRegimeEnum getValue(Integer id) {
        if (id != null) {
            for (TaxationRegimeEnum status : values()) {
                if (id.equals(status.getId())) {
                    return status;
                }
            }
        }
        return null;
    }
}
