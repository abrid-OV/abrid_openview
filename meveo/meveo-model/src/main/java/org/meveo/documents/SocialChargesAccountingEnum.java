package org.meveo.documents;

import javax.inject.Named;

@Named
public enum SocialChargesAccountingEnum {
    CALL_SLIPS_CONTRIBUTION(1, "SocialLoadAccountingEnum.callSLipsContribution"), SETTELMENT_BASE(2, "SocialLoadAccountingEnum.settelmentBase");

    private String label;
    private Integer id;

    SocialChargesAccountingEnum(Integer id, String label) {
        this.label = label;
        this.id = id;
    }

    public String getLabel() {
        return this.label;
    }

    public Integer getId() {
        return this.id;
    }

    public static SocialChargesAccountingEnum getValue(Integer id) {
        if (id != null) {
            for (SocialChargesAccountingEnum status : values()) {
                if (id.equals(status.getId())) {
                    return status;
                }
            }
        }
        return null;
    }
}
