package org.meveo.documents;

public enum DossierStatusEnum {
   NEW("Nouveau"), SUBMITTED("Soumis"), VALIDATED("Validé");
   
    private String label;

    private DossierStatusEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
