package org.meveo.model.admin;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.meveo.model.BusinessEntity;

@Entity
@Table(name = "ACCOUNT_ENTITY", uniqueConstraints = @UniqueConstraint(columnNames = { "code" }))
@SequenceGenerator(name = "ID_GENERATOR", sequenceName = "ACCOUNT_ENTITY_SEQ")
@Inheritance(strategy = InheritanceType.JOINED)
public class AccountEntity extends BusinessEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "ACCOUNT_NAME", length = 100)
	private String name;

	@Embedded
	private Contacts contacts = new Contacts();

	@Column(name = "external_reference", length = 50)
	private String externalReference;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "PRINCIPAL_CONTACT_ID")
	private User principalContact;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "EC_RESPONSIBLE_CONTACT_ID")
	private User responsibleContact;

	@Column(name = "NAME_FOR_SEARCH")
	private String nameForSearch;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ACCOUNT_TYPE_ID")
	private AccountType accountType;

	public User getPrincipalContact() {
		return principalContact;
	}

	public void setPrincipalContact(User principalContact) {
		this.principalContact = principalContact;
	}

	public User getResponsibleContact() {
		return responsibleContact;
	}

	public void setResponsibleContact(User responsibleContact) {
		this.responsibleContact = responsibleContact;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Contacts getContacts() {
		return contacts;
	}

	public void setContacts(Contacts contacts) {
		this.contacts = contacts;
	}

	public String getExternalReference() {
		return externalReference;
	}

	public void setExternalReference(String externalReference) {
		this.externalReference = externalReference;
	}

	public String getNameForSearch() {
		return nameForSearch;
	}

	public void setNameForSearch(String nameForSearch) {
		this.nameForSearch = nameForSearch;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

}