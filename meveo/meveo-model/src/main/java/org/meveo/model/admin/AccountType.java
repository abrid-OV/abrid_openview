package org.meveo.model.admin;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.meveo.model.BaseEntity;

@Entity
@Table(name = "ACCOUNT_TYPE")
@SequenceGenerator(name = "ID_GENERATOR", sequenceName = "ACCOUNT_TYPE_SEQ") 
public class AccountType extends BaseEntity{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public enum AccountTypeEnum {
        serviceProvider,expertComptable,dossier
    };

    public enum NameTypeEnum {
        person, business, both;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "ACCOUNT_TYPE", length = 20)
    private AccountTypeEnum accountTypeEnum;

    @Column(name = "CODE", length = 50)
    private String code;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_ACCOUNT_TYPE_ID")
    private AccountType parentAccountType;

    @Enumerated(EnumType.STRING)
    @Column(name = "NAME_TYPE", length = 20)
    private NameTypeEnum nameTypeEnum = NameTypeEnum.business;

    @Column(name = "FIELDS_OPT", length = 500)
    private String optionalFields;

    @Column(name = "FIELDS_REQ", length = 500)
    private String requiredFields;

    public void setAccountTypeEnum(AccountTypeEnum accountTypeEnum) {
        this.accountTypeEnum = accountTypeEnum;
    }

    public AccountTypeEnum getAccountTypeEnum() {
        return accountTypeEnum;
    }

    public void setCode(String name) {
        this.code = name;
    }

    public String getCode() {
        return code;
    }

    public AccountType getParentAccountType() {
        return parentAccountType;
    }

    public NameTypeEnum getNameTypeEnum() {
        return nameTypeEnum;
    }

    public String getOptionalFields() {
        return optionalFields;
    }

    public String getRequiredFields() {
        return requiredFields;
    }
}