package org.meveo.model.admin;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Contacts implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;
	
    @Column(name = "PHONE", length = 20)
    protected String phone;

    @Column(name = "MOBILE", length = 20)
    protected String mobile;

    @Column(name = "FAX", length = 20)
    protected String fax;

    @Column(name = "SKYPE", length = 20)
    protected String Skype;

    @Column(name = "POSTAL_CODE", length = 5)
    protected String postalCode;

    @Column(name = "CITY", length = 20)
    protected String city;

    @Column(name = "ADDRESS1", length = 255)
    protected String address1;

    @Column(name = "ADDRESS2", length = 255)
    protected String address2;

    @Column(name = "ADDRESS3", length = 255)
    protected String address3;

    public Contacts() {
        super();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getSkype() {
        return Skype;
    }

    public void setSkype(String skype) {
        Skype = skype;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

}
