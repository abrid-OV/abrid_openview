package org.meveo.model.admin;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class BusinessName {

    @Column(name = "BUS_TITLE", length = 30)
    private String title;

    @Column(name = "BUS_NAME", length = 100)
    private String name;

    public BusinessName() {
        super();
        // TODO Auto-generated constructor stub
    }

    public BusinessName(String title, String name) {
        super();
        this.title = title;
        this.name = name;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
