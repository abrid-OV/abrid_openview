package org.meveo.model.admin;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PersonName {

    @Column(name = "PER_TITLE", length = 30)
    private String title;

    @Column(name = "FIRST_NAME", length = 70)
    private String firstName;

    @Column(name = "LAST_NAME", length = 70)
    private String lastName;

    public PersonName() {

    }

    public PersonName(String title, String firstName, String lastName) {
        this.title = title;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public Object getFullName() {
        return firstName + " " + lastName;
    }

    
    
	@Override
	public String toString() { 
		return (title!=null?title:"")+" "+firstName+" "+lastName;
	}
    
    
}
